# 🇯🇵  JP Hash

Python implementation of Kaz Kylheku's JP-Hash.

Passes original `testvec`.

## Requirements

* Python 3.10+

## Installation

1. Copy `jphash` to somewhere in `$PATH`.
2. Done!

## Usage

```
Usage: jphash [OPTION] [WORD …]

positional arguments:
  WORD           calculate JP-hash of WORD

options:
  -h, --help     show this help message and exit
  --version      show version information and exit
  --copyright    show copying policy and exit
  -v, --verbose  print each WORD along with its hash

If no arguments are given on the command line, jphash
will hash individual lines read from standard input.
```

## Examples

```sh
$ jphash neko
Mamya#kige2pyozo
$ echo 'neko' | jphash
Mamya#kige2pyozo
$ cat 1-2-3.txt
ichi
ni
san
$ jphash -v < 1-2-3.txt
Pahe?tomo0gobu     ichi
Toha4chuse@nyaten  ni
Hishosen@ashuge4   san
```

## Testing

Requires `bash`, `awk`, and `diff`.

```sh
$ cd test
$ ./test.sh
Testing Python implementation: PASS!  # Hopefully :)
```

## See also

JP-Hash specification and reference implementations (C, ECMAscript, Lisp): https://www.kylheku.com/cgit/jp-hash/about/
