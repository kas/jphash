#!/usr/bin/env python
"""
Usage: tally_syllables
"""
#  Author: Klaus Alexander Seistrup
# License: GNU General Public License v3+ ⌘ http://gplv3.fsf.org/
#    Date: 2022-10-24

import sys
import collections

# These are the original syllables,
# copied verbatim from the specification:
MORA = [
    "a", "i", "u", "e", "o", "ya", "yu", "yo", "wa",
    "ka", "ki", "ku", "ke", "ko", "ga", "gi", "gu", "ge", "go",
    "sa", "shi", "su", "se", "so", "za", "ji", "zu", "ze", "zo",
    "ta", "chi", "tsu", "te", "to", "da", "de", "do",
    "na", "ni", "nu", "ne", "no", "ha", "hi", "fu", "he", "ho",
    "pa", "pi", "pu", "pe", "po", "ba", "bi", "bu", "be", "bo",
    "ma", "mi", "mu", "me", "mo", "ra", "ri", "ru", "re", "ro",
    "kya", "kyu", "kyo", "gya", "gyu", "gyo", "sha", "shu", "sho",
    "ja", "ju", "jo", "cha", "chu", "cho", "nya", "nyu", "nyo",
    "hya", "hyu", "hyo", "pya", "pyu", "pyo", "bya", "byu", "byo",
    "mya", "myu", "myo", "rya", "ryu", "ryo"
]

# These are the syllables used in jphash:
SYLLABLES = """
a   i   u   e   o   ya  yu  yo  wa
ka  ki  ku  ke  ko  ga  gi  gu  ge  go
sa  shi su  se  so  za  ji  zu  ze  zo
ta  chi tsu te  to  da  de  do
na  ni  nu  ne  no  ha  hi  fu  he  ho
pa  pi  pu  pe  po  ba  bi  bu  be  bo
ma  mi  mu  me  mo  ra  ri  ru  re  ro
kya kyu kyo gya gyu gyo sha shu sho
ja  ju  jo  cha chu cho nya nyu nyo
hya hyu hyo pya pyu pyo bya byu byo
mya myu myo rya ryu ryo
""".split()

N = len(SYLLABLES)  # N=100 (specs says 97?)
try:
    assert len(MORA) == len(SYLLABLES), f'{len(SYLLABLES)=} != {len(MORA)=}'
    assert N == 100, f'expected 100 syllables, found {N}'
    for (index, word) in enumerate(SYLLABLES):
        assert word == MORA[index], f'expected "{MORA[index]}" at {index=}, found "{word}"'
    # We could have asserted SYLLABLES == MORA, but then we wouldn't know the offender.
except AssertionError as error:
    print(error, file=sys.stderr)
    sys.exit(1)

# Assuming that the bytes of the SHA256 digest occur equally frequent
C = collections.Counter(SYLLABLES[i % N] for i in range(256 * 256))
# We have that:
# · 64 syllables each occur 655 out of 65_536 times, while
# · 36 syllables each occur 656 out of 65_536 times:
for (key, val) in C.most_common():
    print(f'{key}\t{val}')

# This skewness will spill over to DIGITS and SYMBOLS,
# but that's an execise for some other time.

sys.exit(0)

# eof
