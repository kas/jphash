In addition to the security considerations mentioned on the JP-Hash
URL, there is also the fact that syllables are not picked with an
equal frequency.

Out of the 100 syllables (specification says 97?):
* 64 syllables each occur 655 out of 65_536 times, while
* 36 syllables each occur 656 out of 65_536 times.

The skewness will probably spill over to the digits and symbols.

See the `tally_syllables.py` script for details.

 // kas@2022-10-24
