#!/usr/bin/env bash

#   Version:  2023-02-08
# Copyright: ©2022 Klaus Alexander Seiﬆrup <klaus@seistrup.dk>
#   License:  GNU Affero General Public License v3+

set -euo pipefail

transmogrify() {
  # Pipes:
  #   1. Print the first column (i.e., the words to hash) from the
  #      testvec file verbatim, except for “EMPTY” which is printed
  #      as an empty string.
  #   2. Convert all words in one fell swoop by invoking jphash
  #      with the “--verbose” (“-v”) switch.
  #   3. Convert the output from jphash to the format of the testvec
  #      file (i.e., “” → “EMPTY”).
  awk '
    $1 != "EMPTY" { print $1; next }
    $1 == "EMPTY" { print "" }
  ' ./testvec \
  | ./jphash --verbose \
  | awk '
      $2 != "" { print   $2 " --> " $1; next }
      $2 == "" { print "EMPTY --> " $1 }
    '
}
export -f transmogrify

printf 'Testing Python implementation: ' >&2

errors=0
diff -uq <(transmogrify) testvec || errors=1

if [ "$errors" -eq 0 ]; then
  printf 'PASS!\n' >&2
else
  printf 'FAIL!\n' >&2
fi

exit $errors

# eof
